#!/bin/sh
set -e

envtemplate(){
	eval "echo \"$(cat "$1")\""
}

mdtitle(){
	TITLE="$(grep '^#\{1,6\}' "$1" | head -n 1 | cut -d'#' -f2)"
	printf "%s" "$TITLE"
}
gitlastedit(){
	TZ=GMT git log -1 --date=rfc --format="%cd" "$1"
}
gitlastedit_pretty(){
	TZ=GMT git log -1 --date=local --date=format:'%Y-%m-%d' --format="%cd" "$1"
}
gitlastcommit(){
	git log -1 --pretty=%B "$1"
}

## Create HTML from Markdown files
rm -f -- *.html
for f in *.md; do
	md_f="$f"
	html_f="$(basename "$f" .md).html"
	
	if command -v gawk >/dev/null; then 
		gawk --posix -f tools/markdown.awk "$md_f" > "$html_f"
	elif command -v nawk >/dev/null; then
		nawk -f tools/markdown.awk "$md_f" > "$html_f"
	fi

	TEMPLATE_POST_TITLE="$(mdtitle "$md_f")"
	TEMPLATE_HTML=$(cat "$html_f")
	export TEMPLATE_POST_TITLE
	export TEMPLATE_HTML
	envtemplate template/post.html > "$html_f"
done


## Create HTML index
rm -f -- index.html
HTML_INDEX=""
for f in *.html; do
	html_f="$f"
	md_f="$(basename "$f" .html).md"

	POST_TITLE="$(mdtitle "$md_f")"
	POST_PRETTY_PUBDATE="$(gitlastedit_pretty "$md_f")"
	
	HTML_INDEX="$HTML_INDEX <li><a href='/posts/$html_f'>$POST_PRETTY_PUBDATE: $POST_TITLE</a> </li>"
done

export TEMPLATE_HTML="<ul>$HTML_INDEX</ul>"
envtemplate template/main.html > index.html


## Create RSS Index
rm -f -- index.rss
RSS_INDEX=""
for f in *.html; do
	html_f="$f"
	md_f="$(basename "$f" .html).md"
	
	test "$html_f" = "index.html" && continue

	POST_TITLE="$(mdtitle "$md_f")"
	POST_LINK="/posts/$html_f"
	POST_DESC="$(gitlastcommit "$md_f")"
	POST_PUBDATE="$(gitlastedit "$md_f")"
	POST_GUID="$(sha512sum "$md_f" | awk '{print $1}')"

	export POST_TITLE
	export POST_LINK
	export POST_DESC
	export POST_PUBDATE
	export POST_GUID

	RSS_ITEM=$(envtemplate template/feed_item.xml)
	RSS_INDEX="$RSS_INDEX $RSS_ITEM"
done

TEMPLATE_RSS="$RSS_INDEX"
export TEMPLATE_RSS
envtemplate template/feed.xml > index.rss
