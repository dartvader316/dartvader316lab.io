/* 
    p316.js 
    CODENAME: 316_11_02

    PUBLIC VERSION

*/

function restrict(restriction,error){
    var body = document.body
    body.innerHTML = ""
    div = document.createElement("div");
    div.classList.add("main_block")
    div.classList.add("error")
    h1 = document.createElement("h1")
    h2 = document.createElement("h2")
    h1.innerHTML = restriction
    h2.innerHTML = "REASON: " + error
    div.appendChild(h1)
    div.appendChild(h2)

    body.appendChild(div)
    
}

function panic(error){

    var body = document.body
    body.innerHTML = ""
    div = document.createElement("div");
    div.classList.add("main_block")
    div.classList.add("error")
    h1 = document.createElement("h1")
    h1.innerHTML = error
    div.appendChild(h1)

    body.appendChild(div)
}


var p316_key = ""

async function fetch_all_blocks(){
    const max_blocks = 2
    p316_key = ""

    for(let i = 0;i<max_blocks;i++){
        p316_key += await fetch_block(i)
    }

    return p316_key

}

async function fetch_block(block) {
    const res = await fetch("/316/static/block/" + block)
    if(!res.ok){
        restrict("RESTRICTED","fetching block " + res.status)
        return
    }
    const text = await res.text()
    return text
}



async function text2download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function xor(input,key) {
    var c = '';
    if (key.length < input.length) {
        restrict("RESTRICTED","input too big.")
        return
    }

    for(let i=0; i<input.length; i++) {
        let value1 = input[i].charCodeAt(0);
        let value2 = key[i].charCodeAt(0);

        c += String.fromCharCode(value1 ^ value2);
    }
    return c;
}


function eval316(s){
    try {
        eval(s);
        return __start316();
    } catch (e) {
        console.log(e)
        return -1
    }
}
